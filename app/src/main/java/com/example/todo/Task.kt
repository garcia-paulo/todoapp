package com.example.todo

class Task constructor(
    private var task: String,
    private var isUrgent: Boolean,
    private var isDone: Boolean,

) {
    var id: Int = 0;
    fun getTask(): String {
        return task
    }
    fun getIsUrgent(): Boolean {
        return isUrgent
    }
    fun getIsDone(): Boolean {
        return isDone
    }
    fun setIsDone(state: Boolean) {
        isDone = state
    }
}