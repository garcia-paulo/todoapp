package com.example.todo

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat

class MainActivity: AppCompatActivity() {
    private var txtNewTask: EditText? = null
    private var swtUrgent: Switch? = null
    private var cbxHideDone: CheckBox? = null
    private var tasksList: LinearLayout? = null


    private var tasksDAO: TaskDAO = TaskDAO

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.txtNewTask = findViewById(R.id.txtNewTask)
        this.swtUrgent = findViewById(R.id.swtUrgent)
        this.cbxHideDone = findViewById(R.id.cbxHideDone)
        this.tasksList = findViewById(R.id.displayLinear)
    }

    private fun buildTaskView(task: Task): View {
        val view = View.inflate(this, R.layout.view_task, null)

        val txtView: TextView = view.findViewById(R.id.txtTaskView)
        txtView.text = task.getTask()
        txtView.background = ContextCompat.getDrawable(this, if (task.getIsUrgent()) R.drawable.red_border else R.drawable.green_border)

        val isDone: CheckBox = view.findViewById(R.id.cbxDone)
        isDone.isChecked = task.getIsDone()

        isDone.setOnClickListener {
            task.setIsDone(isDone.isChecked)
            this.tasksDAO.update(this.tasksDAO.getIndex(task), task)
        }

        return view
    }

    private fun buildTasksList(tasks: List<Task>){
        for (task in tasks){
            val taskView = buildTaskView(task)
            this.tasksList?.addView(taskView)
        }
    }

    fun onClickNew(v: View){
        val valueStr: String = this.txtNewTask?.text.toString()

        if(valueStr.isNotEmpty()) {
            val newTask = Task(valueStr, this.swtUrgent?.isChecked == true, false)
            this.tasksDAO.add(newTask)

            val taskView = buildTaskView(newTask)
            this.tasksList?.addView(taskView)

            this.txtNewTask?.setText("")
        }
    }

    fun onClickHideDone(view: View){
        this.tasksList?.removeAllViews()
        val unfilteredList: ArrayList<Task> = this.tasksDAO.getTasks()

        if(this.cbxHideDone!!.isChecked){
            buildTasksList(unfilteredList.filter{task -> !task.getIsDone()})
        }else{
            buildTasksList(unfilteredList)
        }
    }
}