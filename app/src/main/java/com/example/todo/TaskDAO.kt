package com.example.todo

import com.example.todo.Task

object TaskDAO {
  private val tasks: ArrayList<Task> = ArrayList()
  private var serial: Int = 0;

  fun add(task: Task) {
    task.id = serial++;
    tasks.add(task)
  }

  fun update(index: Int, updatedTask: Task) {
    this.tasks[index] = updatedTask
  }

  fun delete(task: Task){
    this.tasks.removeAt(this.getIndex(task))
  }

  fun getIndex(task: Task) = this.tasks.indexOf(task)

  fun getTasks(): ArrayList<Task> = tasks
}